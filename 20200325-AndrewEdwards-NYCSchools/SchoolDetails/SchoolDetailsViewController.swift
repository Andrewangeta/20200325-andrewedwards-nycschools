//
//  SchoolDetailsViewController.swift
//  20200325-AndrewEdwards-NYCSchools
//
//  Created by Andrew Edwards on 3/26/20.
//  Copyright © 2020 Andrew Edwards. All rights reserved.
//

import UIKit

class SchoolDetailsViewController: UIViewController {
    @IBOutlet var scoreCardView: UIView!
    @IBOutlet var mathScoreLabel: UILabel!
    @IBOutlet var writingScoreLabel: UILabel!
    @IBOutlet var readingScoreLabel: UILabel!
    
    private var school: School
    
    init(school: School) {
        self.school = school
        super.init(nibName: "SchoolDetailsViewController", bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.topItem?.title = "Test Scores 📊"
        loadTestScores()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    func loadTestScores() {
        LoadingView.show()
        SchoolDetails.fetch(dbn: school.dbn) { result in
            DispatchQueue.main.async {
                LoadingView.hide()
                switch result {
                case .failure(.rateLimit):
                    UIAlertController.okAlert(presenter: self,
                                              title: "Rate Limit Hit.",
                                              message: "You've hit a rate limit. Please wait before trying again.")
                    
                case .failure(.default(let message)):
                    UIAlertController.okAlert(presenter: self,
                                              title: "Uh Oh",
                                              message: message)
                    
                case .success(let scores):
                    if let scores = scores {
                        self.mathScoreLabel.text = scores.satMathAvgScore
                        self.writingScoreLabel.text = scores.satWritingAvgScore
                        self.readingScoreLabel.text = scores.satCriticalReadingAvgScore
                    } else {
                        self.mathScoreLabel.text = "N/A"
                        self.mathScoreLabel.textColor = .red
                        self.writingScoreLabel.text = "N/A"
                        self.writingScoreLabel.textColor = .red
                        self.readingScoreLabel.text = "N/A"
                        self.readingScoreLabel.textColor = .red
                        
                        UIAlertController.okAlert(presenter: self,
                                                  title: "No Scores",
                                                  message: "This School has no data for test scores.")
                    }
                }
            }
        }
    }
}

