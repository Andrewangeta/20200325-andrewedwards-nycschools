//
//  SchoolDetails.swift
//  20200325-AndrewEdwards-NYCSchools
//
//  Created by Andrew Edwards on 3/26/20.
//  Copyright © 2020 Andrew Edwards. All rights reserved.
//

import Foundation

struct SchoolDetails: Codable {
    /// The average critical rwading score.
    let satCriticalReadingAvgScore: String
    /// The average math score.
    let satMathAvgScore: String
    /// The average writing score.
    let satWritingAvgScore: String
    
    /// Fetch test scores for a specific school
    /// - Parameters:
    ///   - dbn: The unique identfier of the school whose test scores to retrieve.
    ///   - completion: Closure to call once the operation completes.
    static func fetch(dbn: String, completion: @escaping (Result<SchoolDetails?, APIError>) -> Void) {
        URLSession.shared.dataTask(with: URL(string: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn=\(dbn)")!) { (data, response, error) in
            // Check for errors first
            if let error = error {
                completion(.failure(.default(error.localizedDescription)))
                return
            }
            
            if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                
                if statusCode == 429 {
                    completion(.failure(.rateLimit))
                    return
                }
                
                // Check if it's not 200
                if statusCode != 200 {
                    completion(.failure(.default("An unexpected error occured. Please try again.")))
                    return
                }

                // It has to be 200 so assume data is there.
                do {
                    let jsonDecoder = JSONDecoder()
                    jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
                    
                    let scores = try jsonDecoder.decode([SchoolDetails].self, from: data ?? Data())
                    
                    completion(.success(scores.first))
                } catch {
                    completion(.failure(.default(error.localizedDescription)))
                }
            }
        }.resume()
    }
}
