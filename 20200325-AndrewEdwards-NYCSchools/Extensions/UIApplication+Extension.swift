//
//  UIApplication+Extension.swift
//  20200325-AndrewEdwards-NYCSchools
//
//  Created by Andrew Edwards on 3/26/20.
//  Copyright © 2020 Andrew Edwards. All rights reserved.
//

import UIKit

extension UIApplication {
    var currentWindow: UIWindow? {
        return windows.filter { $0.isKeyWindow }.first
    }
}
