//
//  UIAlertController+Extension.swift
//  20200325-AndrewEdwards-NYCSchools
//
//  Created by Andrew Edwards on 3/25/20.
//  Copyright © 2020 Andrew Edwards. All rights reserved.
//

import UIKit

extension UIAlertController {
    
    static func okAlert(presenter: UIViewController, title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        
        alert.addAction(okAction)
        presenter.present(alert, animated: true, completion: nil)
    }
}
