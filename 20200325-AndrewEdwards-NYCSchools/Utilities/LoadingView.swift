//
//  LoadingView.swift
//  20200325-AndrewEdwards-NYCSchools
//
//  Created by Andrew Edwards on 3/26/20.
//  Copyright © 2020 Andrew Edwards. All rights reserved.
//
import UIKit

public class LoadingView: UIView {
    
    public static let shared = LoadingView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
        backgroundColor = .white
        alpha = 0.7
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        guard let appWindow = UIApplication.shared.currentWindow else { return }
        self.frame = appWindow.bounds
        self.setup()
    }
    
    let loadingLayer = CAShapeLayer()
    
    var loadingEndAnimation: CAAnimation {
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.fromValue = 0
        animation.toValue = 1
        animation.duration = 2
        animation.timingFunction = CAMediaTimingFunction(name: .easeOut)
        animation.isRemovedOnCompletion = false
        
        let group = CAAnimationGroup()
        group.duration = 2.5
        group.repeatCount = Float.infinity
        group.animations = [animation]
        return group
    }
    
    var loadingStartAnimation: CAAnimation {
        let animation = CABasicAnimation(keyPath: "strokeStart")
        animation.fromValue = 0
        animation.toValue = 1
        animation.beginTime = 0.5
        animation.duration = 2
        animation.timingFunction = CAMediaTimingFunction(name: .easeOut)
        animation.isRemovedOnCompletion = false
        
        let group = CAAnimationGroup()
        group.duration = 2.5
        group.repeatCount = Float.infinity
        group.animations = [animation]
        return group
    }
    
    var loadingRotationAnimation: CAAnimation {
        let animation = CABasicAnimation(keyPath: "transform.rotation.z")
        animation.fromValue = 0
        animation.toValue = CGFloat.pi * 2
        animation.duration = 1
        animation.repeatCount = Float.infinity
        animation.isRemovedOnCompletion = false
        return animation
    }
    
    func setup() {
        loadingLayer.lineWidth = 2.5
        loadingLayer.fillColor = nil
        loadingLayer.strokeColor = UIColor.blue.cgColor
        
        let radius = CGFloat(45.0)
        
        let startAngle = CGFloat(-(CGFloat.pi/2))
        
        let endAngle = startAngle + CGFloat(CGFloat.pi * 2)
        
        let path = UIBezierPath(arcCenter: CGPoint.zero, radius: radius, startAngle: startAngle, endAngle: endAngle, clockwise: true)
        
        loadingLayer.position = CGPoint(x: bounds.midX, y: bounds.midY)
        
        loadingLayer.path = path.cgPath
        
        layer.addSublayer(loadingLayer)
        loadingLayer.add(loadingEndAnimation, forKey: "strokeEnd")
        loadingLayer.add(loadingStartAnimation, forKey: "strokeStart")
        loadingLayer.add(loadingRotationAnimation, forKey: "rotation")
    }
    
    /// Shows a full screen spinner view
    static func show() {
        let loader = self.shared

        guard let appWindow = UIApplication.shared.currentWindow else { return }
        loader.frame = appWindow.bounds
        loader.setup()
        appWindow.addSubview(loader)
    }
    
    /// Hides a full screen spinner view.
    static func hide() {
        let loader = self.shared
        if loader.superview != nil {
            loader.removeFromSuperview()
        }
    }
}
