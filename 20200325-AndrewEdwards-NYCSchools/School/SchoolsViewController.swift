//
//  SchoolsViewController.swift
//  20200325-AndrewEdwards-NYCSchools
//
//  Created by Andrew Edwards on 3/25/20.
//  Copyright © 2020 Andrew Edwards. All rights reserved.
//

import UIKit

class SchoolsViewController: UIViewController {
    @IBOutlet var schoolsTableView: UITableView!
    private var dataSource: UITableViewDiffableDataSource<Section, School>!
    private enum Section { case main }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.topItem?.title = "Schools"
        navigationController?.navigationBar.largeTitleTextAttributes = [.foregroundColor: UIColor.label,
                                                                        .font: UIFont.systemFont(ofSize: 42, weight: .heavy)]
        setupTableview()
        loadSchools()
    }
    
    func loadSchools() {
        LoadingView.show()
        School.fetch { res in
            DispatchQueue.main.async {
                LoadingView.hide()
                switch res {
                case .failure(.rateLimit):
                    UIAlertController.okAlert(presenter: self,
                                              title: "Rate Limit Hit.",
                                              message: "You've hit a rate limit. Please wait before trying again.")
                    
                case .failure(.default(let message)):
                    UIAlertController.okAlert(presenter: self,
                                              title: "Uh Oh",
                                              message: message)
                    
                case .success(let schools):
                    var snapshot = NSDiffableDataSourceSnapshot<Section, School>()
                    snapshot.appendSections([.main])
                    snapshot.appendItems(schools)
                    self.dataSource.apply(snapshot, animatingDifferences: false)
                }
            }
        }
    }
    
    func setupTableview() {
        schoolsTableView.register(UINib(nibName: "SchoolCell", bundle: nil), forCellReuseIdentifier: "SchoolCell")
        schoolsTableView.estimatedRowHeight = 200.0
        dataSource = UITableViewDiffableDataSource<Section, School>(tableView: schoolsTableView,
                                                                    cellProvider: { (tableview, index, school) -> UITableViewCell? in
            let cell = tableview.dequeueReusableCell(withIdentifier: "SchoolCell", for: index) as! SchoolCell
            
            cell.schoolNameLabel.text = school.schoolName
            cell.phoneContactLabel.text = "Phone: \(school.phoneNumber ?? "")"
            cell.phoneContactLabel.isHidden = school.phoneNumber == nil
            
            cell.emailContactLabel.text = "Email: \(school.schoolEmail ?? "")"
            cell.emailContactLabel.isHidden = school.schoolEmail == nil
            
            cell.hoursLabel.text = "Hous: \(school.startTime ?? "") - \(school.endTime ?? "")"
            cell.hoursLabel.isHidden = school.startTime == nil || school.endTime == nil
               
            return cell
        })
    }
}

// MARK: - Tableview Delegate
extension SchoolsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let selectedSchool = dataSource.itemIdentifier(for: indexPath) else {
            return
        }
        
        let detailsVC = SchoolDetailsViewController(school: selectedSchool)
        let navigation = UINavigationController(rootViewController: detailsVC)
        DispatchQueue.main.async {
            self.present(navigation, animated: true, completion: nil)
        }
    }
}

class SchoolCell: UITableViewCell {
    @IBOutlet var schoolNameLabel: UILabel!
    @IBOutlet var phoneContactLabel: UILabel!
    @IBOutlet var emailContactLabel: UILabel!
    @IBOutlet var hoursLabel: UILabel!
}
