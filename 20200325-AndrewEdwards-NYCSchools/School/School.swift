//
//  School.swift
//  20200325-AndrewEdwards-NYCSchools
//
//  Created by Andrew Edwards on 3/25/20.
//  Copyright © 2020 Andrew Edwards. All rights reserved.
//

import Foundation

/// Main infrormation about a school.
struct School: Codable, Hashable {
    /// Unique identofier for a school
    let dbn: String
    /// The name of the school
    let schoolName: String
    /// The neighborhood this school is located in
    let neighborhood: String
    /// Phone contact for the school
    let phoneNumber: String?
    /// Email contact for the school
    let schoolEmail: String?
    /// When when classes beign
    let startTime: String?
    /// When classes end
    let endTime: String?
    /// The latitude
    let latitude: String?
    /// The longitude
    let longitude: String?
    
    /// Fetch schools in NYC
    /// - Returns: A collection of `School`s
    static func fetch(_ completion: @escaping (Result<[School], APIError>) -> Void) {
        URLSession.shared.dataTask(with: URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json")!) { (data, response, error) in
            // Check for errors first
            if let error = error {
                completion(.failure(.default(error.localizedDescription)))
                return
            }
            
            if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                
                if statusCode == 429 {
                    completion(.failure(.rateLimit))
                    return
                }
                
                // Check if it's not 200
                if statusCode != 200 {
                    completion(.failure(.default("An unexpected error occured. Please try again.")))
                    return
                }

                // It has to be 200 so assume data is there.
                do {
                    let jsonDecoder = JSONDecoder()
                    jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
                    
                    let schools = try jsonDecoder.decode([School].self, from: data ?? Data())
                    
                    completion(.success(schools))
                } catch {
                    completion(.failure(.default(error.localizedDescription)))
                }
            }
        }.resume()
    }
}

enum APIError: Error {
    case `default`(String)
    case rateLimit
}
